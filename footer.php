<?php
/**
 * This template is for displaying the website's footer and widgets
 *
 * @package Baskerville 2
 */
?>

<?php echo get_joule_footer(); ?>

<?php wp_footer(); ?>

</body>
</html>
