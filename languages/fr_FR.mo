��    ,      |  ;   �      �     �     �  	   �     �     �       8        U     b     q     z     �     �     �     �  _   �  W        r     w  
   �     �  %   �     �     �     �       ;        P     o     �     �     �     �  \   �       
   1  $   <     a     d     y  2   �  T   �     	         7	     N	     Q	     h	     �	     �	  5   �	     �	     �	  	   
     
     %
     C
     R
  $   p
  h   �
  x   �
     w     |     �     �  #   �     �     �     �       >   $     c     f  	   i  !   s     �     �     �     E     b  -   q     �  !   �     �  E   �  N        n                                   *          %   !   	   +   
           )         '              ,            $   #                                                        "   &                           (    %s post %s posts ,  Add yours Author archive Comments are closed. Continue Reading Continue reading %s <span class="meta-nav">&rarr;</span> Contributors Default Header Featured Featured post Footer 1 Footer 2 Footer 3 Full Width, No Sidebar It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Menu Newer Comments No Sidebar Older Comments Oops! That page can&rsquo;t be found. Open Search Pacifico font: on or offon Proudly powered by %s Published by %s Ready to publish your first post? %1$sGet started here%2$s. Roboto Slab font: on or offon Roboto font: on or offon Search Search Results for: %s Sidebar Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Theme: %1$s by %2$s. To the top Try looking in the monthly archives. Up View all posts by %s Website Widgets in this area will be shown in the sidebar. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; http://wordpress.org/ PO-Revision-Date: 2017-02-20 16:24:47+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/2.3.0-rc.1
Language: fr
Project-Id-Version: WordPress.com - Themes - Baskerville-2
 %s article %s articles ,  Ajouter un commentaire Archives d&rsquo;Auteur Les commentaires sont fermés. Lire la suite Lire la suite %s <span class="meta-nav">&rarr;</span> Contributeurs En-tête par défaut à la Une Article mis en avant Pied&nbsp;de&nbsp;page&nbsp;1 Pied de Page 2 Pied&nbsp;de&nbsp;page&nbsp;3 Pleine largeur, sans barre latérale Contenu introuvable. Vous pouvez essayer l'outil de recherche ou les liens ci-dessous pour le retrouver. Il semble que nous ne puissions pas trouver ce que vous cherchez. Le moteur de recherche sera peut-être plus fructueux. Menu Commentaires plus récents Sans Sidebar Commentaires plus anciens Aïe ! Cette page est introuvable. Ouvrir la recherche on Fièrement propulsé par %s Publié par %s Prêt à publier votre premier article? %1$sCommencez ici%2$s. on on Recherche Résultat de recherche pour : %s Colonne latérale Accéder au contenu principal Désolé, mais rien ne correspond à vos termes de recherche. Veuillez essayer de nouveau avec quelques mots-clés différents. Thème&nbsp;: %1$s par %2$s. Retour en haut Essayez de parcourir les archives mensuelles. Retour en haut Afficher tous les articles par %s Site web Les widgets de cette partie seront affichés dans la barre latérale. Un commentaire sur &ldquo;%2$s&rdquo; %1$s commentaires sur &ldquo;%2$s&rdquo; http://wordpress.org/ 