��    -      �  =   �      �     �     �        	                  1  8   B     {     �     �     �     �     �     �     �  _   �  W   @     �     �  
   �     �  %   �     �     �          *  ;   :     v     �     �     �     �     �  \   �     B  
   W  $   b     �     �     �  2   �  T   �     /    E     ^	     t	     �	     �	     �	     �	     �	  3   �	     
     
     
     -
     E
     N
     W
      `
  �   �
  o        t     z     �     �  0   �     �     �     �       G        d     g     j     q     �     �  �   �     /     E  8   T  	   �     �     �  :   �  I   �     @                            #   !       &      $                     -   ,                             '   )                                                  "   
          %   *      +       	          (              %s Edit Post  %s post %s posts ,  Add yours Author archive Comments are closed. Continue Reading Continue reading %s <span class="meta-nav">&rarr;</span> Contributors Default Header Featured Featured post Footer 1 Footer 2 Footer 3 Full Width, No Sidebar It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Menu Newer Comments No Sidebar Older Comments Oops! That page can&rsquo;t be found. Open Search Pacifico font: on or offon Proudly powered by %s Published by %s Ready to publish your first post? %1$sGet started here%2$s. Roboto Slab font: on or offon Roboto font: on or offon Search Search Results for: %s Sidebar Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Theme: %1$s by %2$s. To the top Try looking in the monthly archives. Up View all posts by %s Website Widgets in this area will be shown in the sidebar. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; http://wordpress.org/ PO-Revision-Date: 2017-02-20 16:24:48+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.3.0-rc.1
Language: de
Project-Id-Version: WordPress.com - Themes - Baskerville-2
 %s Beitrag bearbeiten %s Beitrag %s Beiträge ,  Gib deinen ab Autor-Archiv Kommentare sind geschlossen. weiterlesen Weiterlesen %s <span class="meta-nav">&rarr;</span> Autoren Standard-Header Hervorgehoben Hervorgehobener Beitrag Footer 1 Footer 2 Footer 3 Volle Breite, Keine Seitenleiste Sieht so aus, als ob an dieser Stelle nichts gefunden wird. Vielleicht versuchst Du es mit einem der Links unten oder einer Suche? Es sieht so aus, als ob wir nicht das finden konnten, wonach du gesucht hast. Möglicherweise hilft eine Suche. Menü Neuere Kommentare Keine Seitenleiste Ältere Kommentare Diese Seite konnte leider nicht gefunden werden. Suche öffnen on Betrieben von %s Veröffentlicht von %s Bereit, deinen ersten Artikel zu veröffentlichen? %1$sStarte hier%2$s. on on Suchen Suchergebnisse für: %s Seitenleiste Zum Inhalt springen Entschuldigung, aber zu deinen Suchbegriffen wurde nichts passendes gefunden. Bitte versuche es mit anderen Stichworten noch einmal. Theme: %1$s von %2$s. Ganz nach oben Vielleicht findest Du etwas in den monatlichen Archiven. Nach oben Zeige alle Beiträge von %s Website Widgets in diesem Bereich erscheinen in der Seitenleiste.  Ein Kommentar zu &bdquo;%2$s&ldquo; %1$s Kommentare zu &bdquo;%2$s&ldquo; http://wordpress.org/ 