��    *      l  ;   �      �     �     �     �  	   �     �     �     �  8   
     C     L     Z     c     l     u  _   �  W   �     D     I  
   X     c  %   r     �     �     �  ;   �          5     O     V     m     u  \   �     �  
   �  $        '     *     ?  2   G  T   z     �    �     	  !   	     :	     =	     X	     r	     �	  A   �	  
   �	     �	     

     "
     :
  -   R
  �   �
  I     
   V     a     y     �  0   �     �     �     �  =   
     H     L  
   P  #   [          �  {   �  $        D  3   Q  
   �     �     �  C   �  _   �     [     
                              (         #          )              '         %              *            "   !             	                                                $                           &    %s Edit Post  %s post %s posts ,  Add yours Author archive Comments are closed. Continue Reading Continue reading %s <span class="meta-nav">&rarr;</span> Featured Featured post Footer 1 Footer 2 Footer 3 Full Width, No Sidebar It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Menu Newer Comments No Sidebar Older Comments Oops! That page can&rsquo;t be found. Pacifico font: on or offon Proudly powered by %s Published by %s Ready to publish your first post? %1$sGet started here%2$s. Roboto Slab font: on or offon Roboto font: on or offon Search Search Results for: %s Sidebar Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Theme: %1$s by %2$s. To the top Try looking in the monthly archives. Up View all posts by %s Website Widgets in this area will be shown in the sidebar. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; http://wordpress.org/ PO-Revision-Date: 2017-02-22 09:08:17+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: he_IL
Project-Id-Version: WordPress.com - Themes - Baskerville-2
 %sעריכת פוסט הודעה אחת %s הודעות ,  הוסיפו את שלכם ארכיון מחברים התגובות סגורות. להמשיך לקרוא המשך קריאת הפוסט %s <span class="meta-nav">	</span> מובחר רשומה נבחרת תחתית העמוד 1 תחתית העמוד 2 תחתית העמוד 3 רוחב מלא, ללא עמודה צידית לא הצלחנו למצוא שום דבר בכתובת זו. כדאי לנסות את הקישורים הבאים, או את החיפוש. מה שחיפשת לא נמצא. אולי חיפוש יכול לעזור. תפריט תגובות חדשות אין סרגל צידי תגובות ישנות אופס. העמוד שביקשת לא נמצא. off פועל על %s פורסם על ידי %s מוכנים לפרסם פוסט? %1$sהתחילו כאן%2$s off off חיפוש תוצאות חיפוש עבור %s סרגל צדדי דלג לתוכן לא נמצאו תוצאות התואמות לחיפוש. יש לנסות שנית עם מילות מפתח חלופיות. ערכת עיצוב: %1$s של %2$s. להתחלה נסה לחפש בארכיון לפי חודשים. למעלה הצגת הפוסטים של %s אתר ווידג'טים באזור זה יוצגו בסרגל הצדדי. תגובה אחת בנושא &ldquo;%2$s&rdquo; %1$s תגובות בנושא &ldquo;%2$s&rdquo; http://he.wordpress.org/ 