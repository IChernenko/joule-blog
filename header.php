<?php
/**
 * This template is used for the website header
 *
 * @package Baskerville 2
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="icon" href="<?php echo get_template_directory_uri() . '/images/favicon.ico'; ?>" type="image/x-icon" />
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/images/favicon.ico'; ?>" type="image/x-icon" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="header-search-wrap">
    <?php echo get_joule_header(); ?>

    <a href="#search-container" class="screen-reader-text search-toggle"><?php esc_html_e( 'Search', 'baskerville-2' ); ?></a>
    <div class="header-search-block bg-graphite hidden" id="search-container">
        <?php get_search_form(); ?>
    </div> <!-- /header-search-block -->
</div>


<div class="navigation section no-padding bg-dark-blue">
    <nav id="site-navigation" class="navigation-inner section-inner clear" role="navigation">
        <button class="nav-toggle fleft" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'База знань', 'baskerville-2' ); ?></button>
        <div class="main-navigation">
            <?php
            wp_nav_menu( array(
                'container'      => '',
                'theme_location' => 'menu-1',
                'menu_id'        => 'primary-menu',
            ) );
            ?>
        </div>

        <a class="search-toggle search-icon fright" href="#"><?php esc_html_e( 'Open Search', 'baskerville-2' ); ?></a>
    </nav> <!-- /navigation-inner -->
</div> <!-- /navigation -->
