<?php
/**
 * This template is used in the Loop to display posts with the Audio format
 *
 * @package Baskerville 2
 */
?>

<?php if ( ! is_single() ) { ?>
	<div class="post-container">
<?php } ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php
		/**
		 * Post Thumbnail - only show on single.php view
		 */
		if ( is_single() && baskerville_2_has_post_thumbnail() ) { ?>
			<div class="featured-media">
				<?php if ( ! is_single() ) { ?>
					<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
				<?php }
					the_post_thumbnail( 'baskerville-2-post-image' );
				if ( ! is_single() ) { ?>
					</a>
				<?php } ?>
			</div> <!-- /featured-media -->
		<?php }


		/**
		 * Post Title
		 */
		$before_title = '<header class="post-header"><h1 class="post-title entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">';
		$after_title = '</a></h1></header>';
		the_title( $before_title, $after_title );

		/**
		 * Post Content / Excerpt
		 */
		$content = get_the_content();
		if ( ! empty( $content ) ) { ?>

			<div class="post-content clear">
				<?php
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Читати далі %s <span class="meta-nav">&rarr;</span>', 'baskerville-2' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
				wp_link_pages();
				?>
			</div><!--/.post-content-->

		<?php }


		/**
		 * Post Meta
		 */
		if ( is_single() ) { ?>

            <?php
        } else {
			baskerville_2_post_meta();
		} ?>
	</article> <!-- /post -->

<?php if ( ! is_single() ) { ?>
	</div>
<?php } ?>
