jQuery(document).ready(function ($) {
    $('.hamburger').on('click', function () {
       $(this).toggleClass('close');
       $('.header-button').toggleClass('active');
    });

    $('.equipment-btn').on('click', function () {
        $(this).parent().toggleClass('active');
    });
    $(document).on('click', function (e) {
        var container = $(".equipment-dropdown");
        if (container.has(e.target).length === 0) {
            container.removeClass('active');
        }
    });
});